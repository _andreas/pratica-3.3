
import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2 Exemplo de programação em Java.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;

        Matriz transp = orig.getTransposta();
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);

        Matriz matriz2 = new Matriz(2, 2);
        double[][] m2 = matriz2.getMatriz();
        m2[0][0] = 0.1;
        m2[0][1] = 0.5;
        m2[1][0] = 1.5;
        m2[1][1] = 1.0;

        Matriz matriz3 = new Matriz(2, 2);
        double[][] m3 = matriz3.getMatriz();
        m3[0][0] = 0.5;
        m3[0][1] = 1.5;
        m3[1][0] = 1.0;
        m3[1][1] = 2.5;

        Matriz soma = matriz2.soma(matriz3);
        Matriz produto2E3 = matriz2.prod(matriz3);

        System.out.println("Matriz 2: " + matriz2);
        System.out.println("Matriz 3: " + matriz3);

        System.out.println("Soma de Matriz 2 com Matriz 3: " + soma);
        System.out.println("Produto de Matriz 2 com Matriz 3: " + produto2E3);

    }
}
